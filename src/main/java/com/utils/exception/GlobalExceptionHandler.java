package com.utils.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.utils.ResponseEntity;


@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * 全局异常处理，当抛出MyException时返回json格式错误数据
	 * 
	 * @param req
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(value = MyException.class)
	@ResponseBody
	public ResponseEntity jsonErrorHandler(HttpServletRequest req, MyException e) throws Exception {
		return ResponseEntity.fail();
	}
	
	/**
	 * 全局异常处理，当访问路径不存在时返回json格式错误数据
	 * 
	 * @param req
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(value = Exception.class)
	@ResponseBody
	public ResponseEntity urlErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		return ResponseEntity.fail();
	}

}
