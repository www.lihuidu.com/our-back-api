package com.utils;

import java.util.HashMap;
import java.util.Map;

public class ResponseEntity {

	// 状态码，成功200、错误500
	private int code;

	// 错误和成功信息
	private String msg;

	// 包含的数据
	private Map<String, Object> data = new HashMap<>();

	// 成功信息
	public static ResponseEntity success() {
		ResponseEntity msg = new ResponseEntity();
		msg.setCode(200);
		msg.setMsg("处理成功!");
		return msg;
	}

	// 失败信息
	public static ResponseEntity fail() {
		ResponseEntity msg = new ResponseEntity();
		msg.setCode(500);
		msg.setMsg("处理失败!");
		return msg;
	}

	// 添加包含的数据
	public ResponseEntity add(String key, Object value) {
		this.getData().put(key, value);
		return this;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

}
