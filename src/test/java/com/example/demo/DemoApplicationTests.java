package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.utils.CustomProperties;
import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
	
    @Autowired
    private CustomProperties customProperties;

    @Test
    public void test() throws Exception {
    	System.out.println(customProperties.getUsername());
        Assert.assertEquals( "lihuidu",customProperties.getUsername());
    }

}
